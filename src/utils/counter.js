let idCounter = 0;

export default function uniqueId(prefix = '') {
	let id = ++idCounter;
	return String(prefix) + id;
}
